/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function welcomeMessage() {
		let fullName = prompt("What is your name?");
		let age = prompt("Enter your age: ");
		let location = prompt("Where are you located?");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	welcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function myTopFiveArtists(){
		let firstArtist = "Drake";
		let secondArtist = "Giveon";
		let thirdArtist = "Kendrick Lamar";
		let fourthArtist = "Kanye West";
		let fifthArtist = "SZA";

		console.log("1. " + firstArtist);
		console.log("2. " + secondArtist);
		console.log("3. " + thirdArtist);
		console.log("4. " + fourthArtist);
		console.log("5. " + fifthArtist);
	}

	myTopFiveArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myTopFiveMovies() {

		let firstMovie = "Pulp Fiction";
		let secondMovie = "The Truman Show";
		let thirdMovie = "Top Gun: 'Maverick'";
		let fourthMovie = "Midsommar";
		let fifthMovie = "Avatar: The way of water";

		let firstMovieRating = '92%';
		let secondMovieRating = '95%';
		let thirdMovieRating = '96%';
		let fourthMovieRating = '83%';
		let fifthMovieRating = '76%';

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Rating: " + firstMovieRating);
		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Rating: " + secondMovieRating);
		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Rating: " + thirdMovieRating);
		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Rating: " + fourthMovieRating);
		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Rating: " + fifthMovieRating);
	}

	myTopFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	let printFriends = function() {
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	} 
	printFriends();
}

printUsers();
// console.log(friend1);
// console.log(friend2);
